#include<iostream>
#include<cstdio>
typedef long long ll;
using namespace std;
ll rev(ll n)
{
ll rev=0;
while(n>0)
{
rev=rev*10+n%10;
n/=10;
}
return rev;
}
int main()
{
int n;
scanf("%d",&n);
ll n1,n2;
while(n--)
{
scanf("%lld %lld",&n1,&n2);
printf("%lld\n",rev(rev(n1)+rev(n2)));
}
return 0;
}
